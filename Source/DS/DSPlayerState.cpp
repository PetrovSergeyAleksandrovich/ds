// Fill out your copyright notice in the Description page of Project Settings.


#include "DSPlayerState.h"

#include "DSCharacter.h"
#include "DSPlayerController.h"

ADSPlayerState::ADSPlayerState()
{
	bReplicates = true;
}

void ADSPlayerState::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	SendTime(DeltaSeconds);
}

void ADSPlayerState::MulticastSendTime_Implementation(float DeltaSeconds)
{
	SendTime(DeltaSeconds);
}

void ADSPlayerState::SendTime(float DeltaSeconds)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, DeltaSeconds * 100.0f, FColor::Magenta, TEXT("DT: ") + FString::SanitizeFloat(DeltaSeconds, 2));
	}
}

