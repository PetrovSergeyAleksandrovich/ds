// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DSPlayerState.h"
#include "GameFramework/GameModeBase.h"
#include "DSGameModeBase.generated.h"

class ADSPlayerController;
/**
 * 
 */
UCLASS()
class DS_API ADSGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ADSGameModeBase();

	UPROPERTY()
	TArray<APlayerController*> PlayerControllerList;

	UPROPERTY()
	TArray<APlayerState*> PlayerStateList;

	virtual void PostLogin(APlayerController* NewPlayer) override;

	virtual void StartPlay() override;

	int32 GetRoundSecondsRemaining() const { return RoundCountDown; }

	void RespawnRequest(AController* Controller);

private:

	int32 RoundCountDown = RoundTime;
	int32 RoundTime = 90;

	FTimerHandle GameRoundTimerHandle;

	void StartRound();

	void GameTimerUpdate();

	void ResetPlayers();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastResetOnePlayer(ADSPlayerController* PController);
	void MulticastResetOnePlayer_Implementation(ADSPlayerController* PController);

	void ResetOnePlayer(ADSPlayerController* PController);

};

