// Copyright Epic Games, Inc. All Rights Reserved.


#include "DSPlayerController.h"
#include "DSCharacter.h"
#include "EnhancedInputSubsystems.h"

void ADSPlayerController::Reset()
{
	Super::Reset();
}

void ADSPlayerController::SetSpectatorThings()
{
	ViewAPlayer(1);
}

void ADSPlayerController::BeginPlay()
{
	Super::BeginPlay();

	// get the enhanced input subsystem
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		// add the mapping context so we get controls
		Subsystem->AddMappingContext(InputMappingContext, 0);

		UE_LOG(LogTemp, Warning, TEXT("BeginPlay"));
	}
}
