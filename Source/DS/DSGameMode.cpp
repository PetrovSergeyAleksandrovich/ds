// Copyright Epic Games, Inc. All Rights Reserved.

#include "DSGameMode.h"
#include "DSCharacter.h"
#include "UObject/ConstructorHelpers.h"

ADSGameMode::ADSGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;
}
