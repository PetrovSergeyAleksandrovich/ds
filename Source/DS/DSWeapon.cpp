// Fill out your copyright notice in the Description page of Project Settings.


#include "DSWeapon.h"

// Sets default values
ADSWeapon::ADSWeapon()
{
	MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMeshComponent"));
	RootComponent = MeshComp;

	NetUpdateFrequency = 60.0f;
	MinNetUpdateFrequency = 30.0f;

	bReplicates = true;

	MuzzleOffset = FVector(100.0f, 0.0f, 10.0f);

}

// Called when the game starts or when spawned
void ADSWeapon::BeginPlay()
{
	Super::BeginPlay();

	MeshComp->SetSimulatePhysics(true);
}

// Called every frame
void ADSWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

