// Copyright Epic Games, Inc. All Rights Reserved.

// ReSharper disable All
#include "DSCharacter.h"

#include "DSGameMode.h"
#include "DSGameModeBase.h"
#include "DSPlayerController.h"
#include "DSPlayerState.h"
#include "DSProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"
#include "Engine/LocalPlayer.h"
#include "GameFramework/GameModeBase.h"
#include "GameFramework/PlayerState.h"
#include "GameFramework/SpectatorPawn.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

//////////////////////////////////////////////////////////////////////////
// ADSCharacter

ADSCharacter::ADSCharacter()
{
	// Character doesnt have a rifle at start
	bHasRifle = false;
	
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);
		
	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-10.f, 0.f, 60.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	//Mesh1P->SetRelativeRotation(FRotator(0.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-30.f, 0.f, -150.f));

	WeaponInHands = CreateDefaultSubobject<ADSWeapon>(TEXT("WeaponInHands"));

	bReplicates = true;
}



void ADSCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//NETWORK
	if (GetLocalRole() == ENetRole::ROLE_Authority)
	{
		SpawnWeapons();
	}

	//WeaponInHands = Cast<UTP_WeaponComponent>(Arsenal[UKismetMathLibrary::RandomInteger64InRange(0, Arsenal.Num() - 1)]);

	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, true);
	WeaponInHands->AttachToComponent(GetMesh1P(), AttachmentRules, FName(TEXT("GripPoint")));
	SetHasRifle(true);

	Mesh1P->SetVisibility(true);

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(0, 5.0f, FColor::Orange, TEXT("Begin Play ") + GetName());
	}

	ADSPlayerController* PlayerController = Cast<ADSPlayerController>(GetController());
	if (PlayerController)
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->ClearAllMappings();
			Subsystem->AddMappingContext(DefaultMappingContext, 0);

			if (Subsystem && Subsystem->HasMappingContext(DefaultMappingContext))
			{
				if (GEngine)
				{
					GEngine->AddOnScreenDebugMessage(0, 5.0f, FColor::Yellow, TEXT("Applyed Default Mapping Context)"));
				}
			}
		}
	}
}

void ADSCharacter::SpawnWeapons()
{
	WeaponInHands = GetWorld()->SpawnActor<ADSWeapon>(Arsenal[0]);
	if (WeaponInHands)
	{
		WeaponInHands->SetOwner(this);
		AttachWeaponToSocket(WeaponInHands, this->GetMesh1P(), WeaponEquipSocketName);
	}
}

void ADSCharacter::AttachWeaponToSocket(ADSWeapon* Weapon, USceneComponent* SceneComponent, FName& SocketName)
{
	if (!Weapon || !SceneComponent) return;
	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, false);
	Weapon->AttachToComponent(SceneComponent, AttachmentRules, SocketName);
}

//////////////////////////////////////////////////////////////////////////// Input

void ADSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		// Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		// Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ADSCharacter::Move);

		// Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ADSCharacter::Look);

		// Fire
		EnhancedInputComponent->BindAction(FireAction, ETriggerEvent::Triggered, this, &ADSCharacter::Fire);
		// Watch Nest Player
		EnhancedInputComponent->BindAction(NextPlayer, ETriggerEvent::Triggered, this, &ADSCharacter::WatchNextPlayer);

		//Choose weapons
		EnhancedInputComponent->BindAction(WeaponRed, ETriggerEvent::Started, this, &ADSCharacter::SelectPrimaryWeapon);
		EnhancedInputComponent->BindAction(WeaponYellow, ETriggerEvent::Started, this, &ADSCharacter::SelectSecondaryWeapon);
		EnhancedInputComponent->BindAction(WeaponGreen, ETriggerEvent::Started, this, &ADSCharacter::SelectThirdWeapon);

	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input Component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}

void ADSCharacter::SelectPrimaryWeapon()
{
	if (GetLocalRole() < ENetRole::ROLE_Authority)
	{
		ServerSelectPrimaryWeapon();
		return;
	}

	WeaponInHands->Destroy();
	WeaponInHands = GetWorld()->SpawnActor<ADSWeapon>(Arsenal[0]);
	WeaponInHands->GetMesh()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	if (WeaponInHands)
	{
		WeaponInHands->SetOwner(this);
		AttachWeaponToSocket(WeaponInHands, this->GetMesh1P(), WeaponEquipSocketName);
	}
}

void ADSCharacter::ServerSelectPrimaryWeapon_Implementation()
{
	SelectPrimaryWeapon();
}


void ADSCharacter::SelectSecondaryWeapon()
{
	if (GetLocalRole() < ENetRole::ROLE_Authority)
	{
		ServerSelectSecondaryWeapon();
		return;
	}

	WeaponInHands->Destroy();
	WeaponInHands = GetWorld()->SpawnActor<ADSWeapon>(Arsenal[1]);
	WeaponInHands->GetMesh()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	if (WeaponInHands)
	{
		WeaponInHands->SetOwner(this);
		AttachWeaponToSocket(WeaponInHands, this->GetMesh1P(), WeaponEquipSocketName);
	}
}

void ADSCharacter::ServerSelectSecondaryWeapon_Implementation()
{
	SelectSecondaryWeapon();
}

void ADSCharacter::SelectThirdWeapon()
{
	if (GetLocalRole() < ENetRole::ROLE_Authority)
	{
		ServerSelectThirdWeapon();
		return;
	}
	WeaponInHands->Destroy();
	WeaponInHands = GetWorld()->SpawnActor<ADSWeapon>(Arsenal[2]);
	WeaponInHands->GetMesh()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	if (WeaponInHands)
	{
		WeaponInHands->SetOwner(this);
		AttachWeaponToSocket(WeaponInHands, this->GetMesh1P(), WeaponEquipSocketName);
	}
}

void ADSCharacter::ServerSelectThirdWeapon_Implementation()
{
	SelectThirdWeapon();
}


void ADSCharacter::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add movement 
		AddMovementInput(GetActorForwardVector(), MovementVector.Y);
		AddMovementInput(GetActorRightVector(), MovementVector.X);
	}
}

void ADSCharacter::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void ADSCharacter::ServerFire_Implementation()
{
	Fire();
}

bool ADSCharacter::ServerFire_Validate()
{
	return true;
}

void ADSCharacter::Fire()
{

	if (GetLocalRole() < ENetRole::ROLE_Authority)
	{
		ServerFireSound();
		ServerFire();
		return;
	}
	 
	UWorld* const World = GetWorld();

	if (World && GetController())
	{
		APlayerController* PlayerController = Cast<APlayerController>(GetController());
		if (!PlayerController) return;

		const FRotator SpawnRotation = PlayerController->PlayerCameraManager->GetCameraRotation();
		// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
		const FVector SpawnLocation = WeaponInHands->GetActorLocation()  +  SpawnRotation.RotateVector(WeaponInHands->MuzzleOffset) ;

		//Set Spawn Collision Handling Override
		FActorSpawnParameters ActorSpawnParams;
		ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

		// Spawn the projectile at the muzzle
		World->SpawnActor<ADSProjectile>(WeaponInHands->ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);

		for (auto PC : Cast<ADSGameModeBase>(GetWorld()->GetAuthGameMode())->PlayerControllerList)
		{
			PC->ClientPlaySoundAtLocation(WeaponInHands->FireSound, SpawnLocation);
		}

		// Try and play a firing animation if specified
		if (WeaponInHands->FireAnimation != nullptr)
		{
			// Get the animation object for the arms mesh
			UAnimInstance* AnimInstance = GetMesh1P()->GetAnimInstance();
			if (AnimInstance != nullptr)
			{
				AnimInstance->Montage_Play(WeaponInHands->FireAnimation, 1.f);
			}
		}
	}
}


void ADSCharacter::ServerFireSound_Implementation()
{
	FireSound();
}

void ADSCharacter::FireSound()
{
	if (WeaponInHands->FireSound != nullptr)
	{
		UGameplayStatics::PlaySoundAtLocation(this, WeaponInHands->FireSound, WeaponInHands->GetActorLocation());
	}
}

void ADSCharacter::SetHasRifle(bool bNewHasRifle)
{
	bHasRifle = bNewHasRifle;
}

bool ADSCharacter::GetHasRifle()
{
	return bHasRifle;
}

void ADSCharacter::ServerKillCharacter_Implementation()
{
	KillCharacter();
}

void ADSCharacter::KillCharacter()
{
	if(GetLocalRole() < ENetRole::ROLE_Authority)
	{
		BP_EventWhenDead();
		ServerKillCharacter();
	}

	bIsAlive = false;

	if(WeaponInHands)
	{
		WeaponInHands->Destroy();
	}

	GetRootComponent()->SetVisibility(false);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetSimulatePhysics(false);
	GetCapsuleComponent()->SetEnableGravity(false);

	Mesh1P->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	Mesh1P->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	Mesh1P->SetVisibility(false);

	GetMesh()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	GetMesh()->SetCollisionProfileName(FName{ "Ragdoll" }, false);
	GetMesh()->SetVisibility(false);

	ADSPlayerController* PlayerController = Cast<ADSPlayerController>(GetController());
	if (PlayerController)
	{
		UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer());

		if (Subsystem)
		{
			Subsystem->ClearAllMappings();
			Subsystem->AddMappingContext(SpectatorMappingContext, 0);
		}

		if (Subsystem && Subsystem->HasMappingContext(SpectatorMappingContext))
		{
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(0, 5.0f, FColor::Yellow, TEXT("Applyed Spectator Mapping Context)"));
			}
		}

		PlayerController->SetSpectatorThings();
	}

	//ServerMakePlayerToSpectate();
}

void ADSCharacter::SetDeadThings()
{
	if (WeaponInHands)
	{
		WeaponInHands->Destroy();
	}
}


void ADSCharacter::MakePlayerToSpectate()
{
	for (auto PC : Cast<ADSGameModeBase>(GetWorld()->GetAuthGameMode())->PlayerControllerList)
	{
		if (bIsAlive == false)
		{
			Cast<ADSPlayerController>(PC)->ServerViewNextPlayer();
		}
	}
}

void ADSCharacter::ServerWatchNextPlayer_Implementation()
{
	WatchNextPlayer();
}


void ADSCharacter::WatchNextPlayer()
{
	if(GetLocalRole() < ENetRole::ROLE_Authority)
	{
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(0, 1.0f, FColor::Purple, TEXT("Next Player Please"));
		}

		ServerWatchNextPlayer();
	}

	ADSPlayerController* PlayerController = Cast<ADSPlayerController>(GetController());
	if (PlayerController)
	{
		PlayerController->ViewAPlayer(1);
	}
}

void ADSCharacter::ServerMakePlayerToSpectate_Implementation()
{
	MakePlayerToSpectate();
}

void ADSCharacter::OnRep_UpdateHealthBar()
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(0, 1.0f, FColor::Red, TEXT("DAMAGE RECIEVED ") + FString::SanitizeFloat(Health, 2));
	}
}

void ADSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ADSCharacter, Health, COND_OwnerOnly);
}
