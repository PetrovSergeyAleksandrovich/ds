// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "DSPlayerController.generated.h"

class UInputMappingContext;

/**
 *
 */
UCLASS()
class DS_API ADSPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	bool bIsControlActive;

	virtual void Reset() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<ASpectatorPawn> SpectatorCamera;

	void SetSpectatorThings();
	
protected:

	/** Input Mapping Context to be used for player input */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UInputMappingContext* InputMappingContext;

	// Begin Actor interface
protected:

	virtual void BeginPlay() override;

	// End Actor interface
};
