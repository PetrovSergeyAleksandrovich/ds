// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "DSPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class DS_API ADSPlayerState : public APlayerState
{
	GENERATED_BODY()

public:

	ADSPlayerState();

	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(NetMulticast, Reliable)
	void MulticastSendTime(float DeltaSeconds);
	void MulticastSendTime_Implementation(float DeltaSeconds);

	void SendTime(float DeltaSeconds);
};
