// Fill out your copyright notice in the Description page of Project Settings.


#include "DSGameModeBase.h"

#include "DSCharacter.h"
#include "DSPlayerController.h"
#include "DSPlayerState.h"
#include "Kismet/GameplayStatics.h"

ADSGameModeBase::ADSGameModeBase()
{

}

void ADSGameModeBase::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	PlayerControllerList.Add(NewPlayer);
	PlayerStateList.Add(NewPlayer->PlayerState);
}

void ADSGameModeBase::StartPlay()
{
	Super::StartPlay();

	StartRound();
}


void ADSGameModeBase::StartRound()
{
	RoundCountDown = RoundTime;
	GetWorldTimerManager().SetTimer(GameRoundTimerHandle, this, &ADSGameModeBase::GameTimerUpdate, 1.0f, true);
}

void ADSGameModeBase::GameTimerUpdate()
{
	if(GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Emerald, TEXT("Time left ") + FString::SanitizeFloat(RoundCountDown), true);
	}

	if (--RoundCountDown <= 0.0f)
	{
		GetWorldTimerManager().ClearTimer(GameRoundTimerHandle);
		ResetPlayers();
		StartRound();
	}

	for (int32 i =0; i < PlayerControllerList.Num(); i++)
	{
		FVector Loc;
		FRotator Rot;

		PlayerControllerList[i]->GetActorEyesViewPoint(Loc, Rot);

		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Cyan, TEXT("Loc: ") +
				FString{ " x " } + FString::FromInt(Loc.X) +
				FString{ " / y " } + FString::FromInt(Loc.Y) +
				FString{ " / z " } + FString::FromInt(Loc.Z),
				true);
		}
	}
}

void ADSGameModeBase::ResetPlayers()
{
	if (!GetWorld()) return;

	for (auto It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		MulticastResetOnePlayer(Cast<ADSPlayerController>(It->Get()));
	}
}

void ADSGameModeBase::MulticastResetOnePlayer_Implementation(ADSPlayerController* PController)
{
	ResetOnePlayer(PController);
}

void ADSGameModeBase::ResetOnePlayer(ADSPlayerController* PController)
{
	if (PController && PController->GetPawn())
	{
		Cast<ADSCharacter>(PController->GetPawn())->SetDeadThings();
		PController->GetPawn()->Reset();
	}

	RestartPlayer(PController);
}
